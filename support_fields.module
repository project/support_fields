  <?php

/**
 * @file
 * Support fields
 */


/**
 * Implementation of hook_init() to add css for the node-edit form
 * to prevent the assigned field from breaking the layout.
 * @TODO: copied as is from the D6 version, it may need to be adjusted as per D7
 */
function support_fields_init() {
	drupal_add_css(drupal_get_path('module', 'support_fields') .'/support_fields.css', array('type' => 'file', 'media' => 'all', 'preprocess' => FALSE ));
}



/**
 * Provide some inline documentation.
 */

function support_fields_help($path, $arg) {
  switch ($path) {
    case 'admin/support/clients/manage/%/fields':
      $output = '<p>' . t('These settings allow you to control which CCK fields of the support_ticket content type is available for this Client. You can add more fields via CCK to the content type. Check the checkbox next to the fields you wish to use together with this Client.') . '</p>';
      break;
    default:
      $output = '';
      break;
  }
  return $output;
}


/**
 * Implementation of hook_menu() to add the paths and menu items.
 *
 **/
function support_fields_menu() {
	$items = array();
	
	// Path to edit client
	$items['admin/support/clients/manage/%/fields'] = array(
    'title' => 'Manage Support Fields',
    'type' => MENU_LOCAL_TASK,
  	  'page callback' => 'drupal_get_form',
    	'page arguments' => array('support_fields_admin_client'),
    'access arguments' => array('administer support'),
  );
  
    // Setup "fast links" to be able to create Client specic
  // support tickets directly from the node/add page.
  
  // Load the Clients
  $clients = support_fields_active_clients();
  if (is_array($clients)) {
    // If there are Clients, loop through
    foreach ($clients as $clid => $client) {
      // Check Client setting if it's to be listed
    	if (variable_get('support_fields_' . $client->clid . '_content_type', FALSE) == TRUE) {
    	 // Create path
				$items['node/add/support_ticket/' . $client->clid] = array(
  				'title' => t($client->name),
				'title callback' => 'check_plain',
     		'page callback' => 'custom_edit', // Special callback for the node-edit form
      		'page arguments' => array(2, $client->clid),
      		'access callback' => 'node_access',
      		'access arguments' => array('create', 'support_ticket'),
				 'description' => t(variable_get('support_fields_' . $client->clid . '_content_type_description' , 'This creates a ' . $client->name . ' type support ticket.')),
  		  ); 
      }
    }
  }
	return $items;	
}

/**
 *  Custom page callback for node-edit form for "fast linked" Clients.
 *  Includes the node.pages.inc since we are working with the node form.
 */
function custom_edit($type, $clid) {
  // Require the file and return
	require_once(drupal_get_path('module', 'node') . '/node.pages.inc');
  return node_add($type);
}


/**
 * Settings form for the Support fields.
 *
 */
function support_fields_admin_client() {
	$form = array();	
  	
	// Load the Client
  $client = support_client_load(arg(4), TRUE);

	// Get all content types for the content-type
  $type = field_info_instances("node",'support_ticket');
  	
	//@TODO Remove default fields from the list of fields.

	// Loop through all the fields and add them to the form
	foreach($type as $field_name => $field) {
		$form['support_fields_' . $client->clid . '_' . $field_name] = array(
	    '#title' => t($field['label']),
	    '#type' => 'checkbox',
	    '#default_value' => variable_get('support_fields_' . $client->clid . '_' . $field_name , FALSE),
	    '#description' => t('Check this box to include this field in this client.'),
	 	);
  }

  // Create a wrapper for "fast link" options
  $form['content_type'] = array(
		'#title' => t('As content type'),
		'#type' => 'fieldset',
		'#collapsed' => FALSE,
		'#collapsible' => TRUE,
	);
	
	// Add a selection box for showing on the node/add page
	$form['content_type']['support_fields_' . $client->clid . '_content_type'] = array(
	  '#title' => t('Display on "Create content" page.'),
	  '#type' => 'checkbox',
	  '#default_value' => variable_get('support_fields_' . $client->clid . '_content_type' , FALSE),
	  '#description' => t('Check this box to display this client as a default choice to the support_ticket content type.'),
	);
	
	// Add a textfield for a description
	$form['content_type']['support_fields_' . $client->clid . '_content_type_description'] = array(
	  '#title' => t('Client description.'),
	  '#type' => 'textfield',
	  '#default_value' => variable_get('support_fields_' . $client->clid . '_content_type_description' , FALSE),
	  '#description' => t('Type in a short description for the Client that is being displayed on the "Create content" page.'),
	);
	$form['#submit'] = array('support_fields_admin_client_submit');
	return system_settings_form($form);
}


/**
 *	Submit handler for the admin client form to rebuild menu and
 *  set the variable to remember which Client the field belongs to.
 * @CAUTION : Menu rebuild has consequences as it may slow down the website
 */
function support_fields_admin_client_submit($form, &$form_state) {
  if(is_numeric(arg(4)) && arg(4) > 0) {
  	variable_set('support_fields_' . arg(4) . '_content_type' , $form_state['values']['support_fields_' . arg(4) . '_content_type']);
  }
  menu_rebuild();
}


/**
 * Implements of hook_node_view() 
 * It is here used to hide unwanted fields from display.
 */
 
function support_fields_node_view($node, $view_mode, $langcode) {
  
  if($node->type == 'support_ticket') {
    // Get all fields
    $type = field_info_instances("node",'support_ticket');
   
    // Get the Client
    $client = support_client_load($node->client, TRUE);

    // Loop through all fields
    foreach($type as $field_name => $field) {
      // If it doesn't belong, hide it
      if(variable_get('support_fields_' . $client->clid . '_' . $field_name , FALSE) == FALSE) {
        hide($node->content[$field_name]);
      }
    }
  }
}


/**
 * Load all active clients.
 */
function support_fields_active_clients() {
  static $clients = NULL;

  if (is_null($clients)) {
    $result = db_query('SELECT * FROM {support_client} WHERE status = 1');
    foreach ($result as $client) {
      $clients[$client->clid] = $client;
    }
  }
  return $clients;
}

/**
 * Implementation of hook_form_alter() on the node-edit / node-add form for support module.
 *
 */
function support_fields_form_alter(&$form, &$form_state, $form_id) {

  if($form_id == 'support_ticket_node_form') {
        //Hide the form fields that needs to be removed from the client form      
      $type = field_info_instances("node",'support_ticket');
      
    // Shortcut node form from /node/add page, use the clid from arg() and lock down selection
    if(is_numeric(arg(3)) && arg(3) > 0) {
      $clid = arg(3);
      $form['support']['client']['#default_value'] = $clid;
      $form['support']['client']['#disabled'] = TRUE;
     
      // Get the Client
      $client = support_client_load($clid, TRUE);

      // Loop through all fields
      foreach($type as $field_name => $field) {
        // If it doesn't belong, hide it
        if(variable_get('support_fields_' . $client->clid . '_' . $field_name , FALSE) == FALSE) {
          $form[$field_name][$form[$field_name]['#language']]['#required'] = 0;
          unset($form[$field_name]);
        }
      }      
    }
    
    //Override AJAX callback for client form
    $form['support']['client']['#ajax']['callback'] = '_overridden_support_ajax_update_assigned_callback';
    

    // Setting inital client value
    if (!isset($form_state['values']['client'])) { // Nothing save yet
      $clients = _support_available_clients();
        if (sizeof($clients) == 1) {
          $client = key($clients);
        }    
        else {
          if($form['support']['client']['#default_value'] > 0)
            $client = $form['support']['client']['#default_value']; // Getting from default form value
          else
            $client = 0; // No default form value, setting to 0        
        }
    }
    else {
      $client = $form_state['values']['client']; // Client was saved so bringing it back
    }    
    
    // Set up the support wrapper to contain the AJAX magic and put it at top of form
    $form['support']['#weight'] = -10;
    $form['support']['#prefix'] = '<div id="support-fields-wrapper">'; // Wrapper for styling
    $form['support']['#suffix'] = '</div>';
    
    // Change the client around a bit, position if first for UI purpose and add some magic
    $form['support']['client']['#default_value'] = $client;
    $form['support']['client']['#weight'] = -10;
    $form['support']['client']['#prefix'] = '<div class="container-inline">'; // Keeps containers inline, ends before assigned
    $form['support']['client']['#suffix'] = '&nbsp;&nbsp;';  
    
    // Position the rest of the support controls
    $form['support']['state']['#weight'] = -9;
    $form['support']['priority']['#weight'] = -8;
    $form['support']['assigned']['#prefix'] = '<br/>';
    $form['support']['assigned']['#weight'] = -7;
    $form['support']['assigned']['#suffix'] = '</div>';
    
     // Set up a wrapper
    $form['support']['support-fields-ajax-wrapper'] = array(
      '#prefix' => '<div id="support-fields-ajax-wrapper">',
      '#suffix' => '</div>',
      '#type' => 'fieldset',
      '#description' => t('Please fill out these values.'),
    );    
    
    // Loop through fields and put them into the magic box (the wrapper)
    foreach($type as $fields => $field) {
      if(variable_get('support_fields_' . $client . '_' . $field['field_name'], FALSE) == TRUE) { // Field exists in the given client
        if($form[$field['field_name']] != NULL) {
          $form['support']['support-fields-ajax-wrapper'][$field['field_name']] = $form[$field['field_name']];
        }   
      }
      unset($form[$field['field_name']]); // Remove all fields that are not activated
    }
  }
}


/**
 *
 */
function _overridden_support_ajax_update_assigned_callback($form, &$form_state) {

  return array(
    '#type' => 'ajax',
    '#commands' => array(
      ajax_command_replace("#replace_support_client_dependencies", render($form['support']['client_dependencies'])),
      ajax_command_replace("#support-fields-ajax-wrapper", render($form['support']['support-fields-ajax-wrapper'])),
    )
  );

  return ;
}
